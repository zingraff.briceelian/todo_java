package fr.afpa.brice.todoApp.daoJDBC;

import java.sql.SQLException;
import java.util.List;

import fr.afpa.brice.todoApp.model.Todo;

public interface TodoDao {

	 void insertTodo(Todo todo) throws SQLException;

	 Todo selectTodo(long todoId);

	 List<Todo> selectAllTodos();

	 boolean deleteTodo(int id) throws SQLException;

	 boolean updateTodo(Todo todo) throws SQLException;

	}
